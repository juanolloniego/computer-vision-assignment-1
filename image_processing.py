import os
import cv2
import sys
import math
import numpy as np


def is_odd(number):
    return bool(number & 1)


def template_convolution(image, template):
    template_height = template.shape[0]
    template_width = template.shape[1]

    assert(is_odd(template_height))
    assert(is_odd(template_width))

    # Get Pad measures for the Image (instead of black border)
    pad_height = int(template_height / 2)
    pad_width = int(template_width / 2)
    pad_parameters = [(pad_height, pad_height), (pad_width, pad_width)]

    image_height = image.shape[0]
    image_width = image.shape[1]
    image_layers = 1  # Assume Grayscale first
    if len(image.shape) > 2:
        # Not a Grayscale
        image_layers = image.shape[2]
        pad_parameters.append((0, 0))  # Do not pad on the layer domain

    # Pad image with measures from above. Edge mode copies the value at the edge and pads with it.
    # Modes: symmetric, reflect and mean also worked fine.
    padded_image = np.pad(image, pad_parameters, mode='edge')
    if image_layers == 1:
        # If image is grayscale put each value inside an array to make code work with no changes
        padded_image = padded_image.reshape(padded_image.shape + (1,))

    # Create empty result image with same shape as original.
    result_image = np.zeros((image_height, image_width, image_layers), dtype=np.double)

    for x in range(image_height):
        for y in range(image_width):
            for layer in range(image_layers):
                # Get the current image window for each layer
                image_window = padded_image[x:(x + template_height), y:(y + template_width), layer]
                # Multiply the window and the template (element-wise).
                convolution = np.multiply(image_window, template)
                # Add all the values from the result of the previous multiplication and set it to the resulting image
                result_image[x, y, layer] = np.sum(convolution)

    return result_image


def gaussian_template(sigma):
    # Get the appropriate size for the template from the sigma value
    size = int(8 * sigma + 1)
    if not is_odd(size):
        size += 1

    template_centre = int(size / 2)

    result_template = np.zeros((size, size), dtype=np.double)

    accumulator = 0  # Divide by the sum of all values
    power_denominator = 2 * sigma * sigma
    base_denominator = 2 * math.pi * sigma * sigma
    for x in range(size):
        for y in range(size):
            numerator = ((x - template_centre) * (x - template_centre)) + ((y - template_centre) * (y - template_centre))
            value = math.exp(-(numerator / power_denominator)) / base_denominator
            accumulator += value
            result_template[x][y] = value

    return np.divide(result_template, accumulator)


def create_hybrid_image(image_one, image_two, sigma_low, sigma_high):
    # If sizes are not the same we cannot create hybrid.
    assert image_one.shape == image_two.shape, "Image sizes are not compatible."

    template = gaussian_template(sigma=sigma_low)
    low_pass_one = template_convolution(image_one, template)
    cv2.imwrite('results/low_pass_one.png', low_pass_one)  # Save it in results folder.

    template = gaussian_template(sigma=sigma_high)
    low_pass_two = template_convolution(image_two, template)
    cv2.imwrite('results/low_pass_two.png', low_pass_two)  # Save it in results folder.

    high_pass_two = image_two - (low_pass_two.reshape(image_two.shape))
    cv2.imwrite('results/high_pass_two.png', high_pass_two)  # Save it in results folder.

    high_pass_pretty = np.add(high_pass_two, 128)  # Create a nicer visualization image for the high pass.
    cv2.imwrite('results/high_pass_vis.png', high_pass_pretty)  # Save it in the results folder.

    hybrid = high_pass_two + low_pass_one.reshape(image_one.shape)
    cv2.imwrite('results/hybrid.png', hybrid)  # Save it in results folder.
    return hybrid


def create_down_sampling_image(hybrid):
    # Generates a progressing down-sampling image of the hybrid and save it.
    multipliers = [2, 3, 4, 6]
    sizes = [int(hybrid.shape[1] / s) for s in multipliers]

    shape = (hybrid.shape[0], hybrid.shape[1] + np.sum(sizes) + (len(sizes) + 1) * 5)
    if len(hybrid.shape) > 2:
        shape += (hybrid.shape[2],)

    result = np.full(shape, 255, dtype=np.double)

    # Add original image to the result
    result[0: hybrid.shape[0], 0:hybrid.shape[1]] = hybrid
    offset = hybrid.shape[1] + 5

    for multiplier in multipliers:
        new_img = cv2.resize(hybrid, None, fx=1/multiplier, fy=1/multiplier, interpolation=cv2.INTER_AREA)
        result[result.shape[0] - new_img.shape[0]:result.shape[0], offset:offset + new_img.shape[1]] = new_img
        offset += new_img.shape[1] + 5

    cv2.imwrite('results/down_sampling.png', result)  # Save the result
    return result


def run(low_pass_path, high_pass_path, sigma_low, sigma_high, read_type):
    low_pass = cv2.imread(low_pass_path, read_type)
    high_pass = cv2.imread(high_pass_path, read_type)

    # Call the creation method with the selected inputs.
    hybrid_image = create_hybrid_image(low_pass, high_pass, sigma_low=sigma_low, sigma_high=sigma_high)

    # Create the down-sampled image.
    create_down_sampling_image(hybrid_image)


if __name__ == '__main__':
    if len(sys.argv) < 5:
        # Execute the code with two pre loaded images.
        run('images/dog.bmp', 'images/cat.bmp', sigma_low=8, sigma_high=8, read_type=1)
    else:
        # Take the first two arguments only as input images.
        name1 = os.curdir + os.sep + sys.argv[1]  # Low Pass
        name2 = os.curdir + os.sep + sys.argv[2]  # High Pass
        sigma1 = int(sys.argv[3])
        sigma2 = int(sys.argv[4])
        read = int(sys.argv[5])  # 1 for Colour and 0 for grayscale

        if read not in [0, 1]:
            read = 1  # Default to Colour

        run(name1, name2, sigma1, sigma2, read)
