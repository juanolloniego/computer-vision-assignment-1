\documentclass[12pt,oneside]{book}
\usepackage[a4paper,
            left=1in,right=1in,top=1in]{geometry}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{listingsutf8}
\usepackage{comment}
\usepackage{float}
\usepackage{longtable}
\usepackage[hidelinks]{hyperref}

\usepackage{listings}
\usepackage{color}

% Custom colors
\definecolor{deepblue}{rgb}{0,0,0.5}
\definecolor{deepgreen}{rgb}{0,0.5,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}

\lstset{frame=tb,
  language=Python,
  breaklines=true,
  basicstyle=\small,
  otherkeywords={self, def, assert},
  keywordstyle=\color{deepblue},
  stringstyle=\color{deepgreen},
  commentstyle=\color{gray},
  showstringspaces=false,
  columns=flexible,
  numbers=none,
  tabsize=1
}

\usepackage{cite}

%%posición de la numeración
\usepackage{fancyhdr}
\fancypagestyle{plain}{
  \fancyhf{}
  \fancyfoot[RO]{\thepage}
}
\fancyhf{}
\fancyfoot[RO]{\thepage}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\pagestyle{fancy}

%%para incluir imagenes
\usepackage{graphicx}
\graphicspath{{"/home/juan/Desktop/MSC/Semester 1/Computer Vision/Assignment 1/report_images/"}} 

%%para la bibliografía
\AtBeginDocument{%
  \renewcommand\bibname{References}
}

%%para los capítulos
\makeatletter
\def\@makechapterhead#1{
  \vspace*{50\p@}
  {\parindent \z@ \raggedright \normalfont
    \ifnum \c@secnumdepth >\m@ne
        \huge\bfseries \space \thechapter\space
    \fi
    \interlinepenalty\@M
    \Huge \bfseries #1\par\nobreak
    \vskip 40\p@
  }}
  \makeatother
 
\begin{document}

\begin{center}

\begin{Huge}
\textbf{Coursework 1: Image Filtering and Hybrid Images} \\[0.2in]
\end{Huge}
\begin{Large}
Juan Andres Olloniego - University of Southampton\\
\end{Large}
\end{center}

\bigskip

In this report I will discuss my approach to solving the first coursework assignment for the Computer Vision module. I will discuss the following: 
\begin{itemize}
	\item My convolution algorithm
	\item My hybrid images algorithm
	\item Extra work done
	\item Running the code
\end{itemize}

\bigskip

\begin{Large}
\textbf{Convolution Algorithm} \\[0.1in]
\end{Large}
The implementation of my convolution algorithm uses matrix dot multiplication and then adds up all the values from the resulting matrix to compute the value of a new point in the resulting image. To do this in python I took advantage of Numpy\cite{numpyPad} and its built in functions \textit{multiply} and \textit{sum}, as well as Numpy arrays and indexing; in particular, the ability to get a region from a matrix (i.e. the image window to compute at a given point in time).\\
Another thing I did for my convolution algorithm was to pad the input image instead of setting the border to black in order to get a better-looking end result. To do this I used Numpy's \textit{pad} function that takes a list of parameters indicating how much to pad in each direction, the input image and a pad 'mode'. This mode can be multiple things, I tested \textit{constant values}, \textit{symmetric}, \textit{reflect}, \textit{mean} and \textit{edge}; the output was very similar for the last four, with edge being the fastest one to compute (which only pads with the edge values of the matrix). \\
Finally, the algorithm was coded in such a way that works for grayscale and colour images without modification or abuse of \textit{if} statements, this was done using the \textit{shape} and \textit{reshape} functionalities of Numpy so that every image processed has a shape of (x, y, z) where x is the height, y the width and z the 'depth' or 'layers' of the image (1 for grayscale and 3 for RGB colour), in the case of grayscale, where the original image is only of the form (x, y) it gets \textit{reshaped} to the form (x, y, 1).\\
As for the execution of the algorithm, it goes as follows: for every pixel in every 'layer' of the original image, it calculates the dot product of the window in the padded input image with the template and sets the result of adding up all those values to a point in the resulting image.

The code for this function is based on the ideas presented in the lectures as well as Mark Nixon's book \cite{marksBook} and is as follows:

\newpage

\begin{lstlisting}[language=Python]
def template_convolution(image, template):
    template_height = template.shape[0]
    template_width = template.shape[1]

    assert(is_odd(template_height))
    assert(is_odd(template_width))

    # Get Pad measures for the Image (instead of black border)
    pad_height = int(template_height / 2)
    pad_width = int(template_width / 2)
    pad_parameters = [(pad_height, pad_height), (pad_width, pad_width)]

    image_height = image.shape[0]
    image_width = image.shape[1]
    image_layers = 1  # Assume Grayscale first
    if len(image.shape) > 2:
        # Not a Grayscale
        image_layers = image.shape[2]
        pad_parameters.append((0, 0))  # Do not pad on the layer domain

    # Pad image with measures from above. Edge mode copies the value at the edge and pads with it.
    # Modes: symmetric, reflect and mean also worked fine.
    padded_image = np.pad(image, pad_parameters, mode='edge')
    if image_layers == 1:
        # If image is grayscale put each value inside an array to make code work with no changes
        padded_image = padded_image.reshape(padded_image.shape + (1,))

    # Create empty result image with same shape as original.
    result_image = np.zeros((image_height, image_width, image_layers), dtype=np.double)

    for x in range(image_height):
        for y in range(image_width):
            for layer in range(image_layers):
                # Get the current image window for each layer
                image_window = padded_image[x:(x + template_height), y:(y + template_width), layer]
                # Multiply the window and the template (element-wise).
                convolution = np.multiply(image_window, template)
                # Add all the values from the result of the previous multiplication and set it to the resulting image
                result_image[x, y, layer] = np.sum(convolution)

    return result_image
\end{lstlisting}

\newpage

\begin{Large}
\textbf{Hybrid Images Algorithm} \\[0.1in]
\end{Large}
For this algorithm I made use of the convolution function and the Gaussian template generator. Since the Gaussian template is symmetric there's no point in flipping it to do the convolution, thats why it's never flipped before doing convolution (the algorithm assumes the template is already flipped when it receives it as a parameter).\\
The algorithm receives as input: two images (one to be low-passed and one to be high-passed) and two sigma values (one for each pass). With this it creates two Gaussian templates from the sigma values (the size of the templates is given by the formula: int(8 * sigma + 1)\cite{coursework} and made odd by adding 1 when its even).\\
After this it creates a low-pass version of each image by doing template convolution with the corresponding Gaussian template (the low-pass version is ready after this). To obtain the high-pass version I subtract the low-pass version of the image from itself. Finally I just add the low-pass and high-pass versions to create the hybrid.\\
Examples of the algorithm results:

\begin{figure}[H]
\centering
\begin{minipage}[t]{.5\textwidth}
  \centering
  \includegraphics[scale=0.5]{niceCatDog.png}
  \caption{Cat and dog hybrid}
  \label{fig:niceCatDog1}
\end{minipage}%
\begin{minipage}[t]{.5\textwidth}
  \centering
  \includegraphics[scale=0.5]{niceFishSub.png}
  \caption{Fish and submarine hybrid}
  \label{fig:fishSubmarine}
\end{minipage}
\end{figure}

\begin{figure}[H]
	\includegraphics[scale=0.7]{downSampling.png}
	\caption{Einstein turning into Marilyn Monroe}
	\label{fig:downSampling}
\end{figure}

\newpage

\begin{Large}
\textbf{Extra work done} \\[0.1in]
\end{Large}
As for extra work, I created a down sampling function that saves a new image containing various sizes of the hybrid result. This way it's easier to visualize the change in it. This function is easily customizable to include different sampling factors.\\
In addition to this, the hybrid creating function also saves copies of the low pass, high pass and enhanced visibility high pass of the images processed.\\
Furthermore, all the code allows reading images in RGB Mode and Grayscale Mode (i.e. Loading any image as graysclale) with no modification.

\bigskip

\begin{Large}
\textbf{Running the code} \\[0.1in]
\end{Large}
To run the code you'll need the following python packages: Numpy and OpenCV\cite{openCv} (just used for image loading, saving and resizing), as well as Python 3.\\
After having istalled this dependencies running the code is just executing the python script itself:
\textit{python image\_processing.py} this will create a cat dog hybrid with default sigma values of 8 for both.\\
You can also run it with custom images and sigmas like this:\\ \textit{python image\_processing.py lowPassPath highPassPath sigmaLow sigmaHigh colour}.

\bigskip

To read the image in RGB mode the value if 1, for Grayscale it's 0.\\
Example: \textit{python image\_processing.py /images/dog.bmp /images/cat.bmp 5 7 1}


\bibliographystyle{IEEEtran}
\bibliography{biblio}

\newpage

\begin{Large}
\textbf{Appendix: More Images} \\[0.1in]
\end{Large}

\begin{figure}[H]
\centering
\begin{minipage}[t]{.5\textwidth}
  \centering
  \includegraphics[scale=0.5]{lowPassDog.png}
  \caption{Low pass version of the dog}
  \label{fig:lowPassDog}
\end{minipage}%
\begin{minipage}[t]{.5\textwidth}
  \centering
  \includegraphics[scale=0.5]{highPassCat.png}
  \caption{High pass version of the cat with improved visibility}
  \label{fig:highPassCat}
\end{minipage}
\end{figure}

\begin{figure}[H]
	\includegraphics[scale=0.5]{catDogTransition.png}
	\caption{Showcasing the effect of combining the cat and the dog images}
\end{figure}

\begin{figure}[H]
	\includegraphics[scale=0.5]{uglyFishSub.png}
	\caption{Effect of low-passing the fish and combining it with the submarine. Opposite to Figure \ref{fig:fishSubmarine} }
\end{figure}

The following images show the intermediate steps in making the above fish and submarine hybrid.

The low pass versions:
\begin{figure}[H]
\centering
\begin{minipage}[t]{.5\textwidth}
  \centering
  \includegraphics[scale=0.5]{lpFish.png}
\end{minipage}%
\begin{minipage}[t]{.5\textwidth}
  \centering
  \includegraphics[scale=0.5]{lpSub.png}
\end{minipage}
\end{figure}

The high pass version of the submarine (with 128 added to each pixel value for a nicer visualization):
\begin{figure}[H]
\centering
\begin{minipage}[t]{.5\textwidth}
  \centering
  \includegraphics[scale=0.5]{hpSubReal.png}
\end{minipage}%
\begin{minipage}[t]{.5\textwidth}
  \centering
  \includegraphics[scale=0.5]{hpSubNice.png}
\end{minipage}
\end{figure}


\newpage

\begin{figure}[H]
	\includegraphics[scale=0.5]{planeBird.png}
	\caption{Effect of low-passing the plane and combining it with the bird. }
	\label{fig:planeBird}
\end{figure}

The following images show the intermediate steps in making the above plane and bird hybrid.

The low pass versions:
\begin{figure}[H]
\centering
\begin{minipage}[t]{.5\textwidth}
  \centering
  \includegraphics[scale=0.5]{lpPlaneOne.png}
\end{minipage}%
\begin{minipage}[t]{.5\textwidth}
  \centering
  \includegraphics[scale=0.5]{lpBirdOne.png}
\end{minipage}
\end{figure}

The high pass version of the bird (with 128 added to each pixel value for a nicer visualization):
\begin{figure}[H]
\centering
\begin{minipage}[t]{.5\textwidth}
  \centering
  \includegraphics[scale=0.5]{hpBirdOne.png}
\end{minipage}%
\begin{minipage}[t]{.5\textwidth}
  \centering
  \includegraphics[scale=0.5]{hpBirdOneNice.png}
\end{minipage}
\end{figure}

\begin{figure}[H]
	\includegraphics[scale=0.5]{birdPlane.png}
	\caption{Effect of low-passing the bird and combining it with the plane. Opposite of Figure \ref{fig:planeBird}}
\end{figure}

The following images show the intermediate steps in making the above bird and plane hybrid.

The low pass versions:
\begin{figure}[H]
\centering
\begin{minipage}[t]{.5\textwidth}
  \centering
  \includegraphics[scale=0.5]{lpPlaneTwo.png}
\end{minipage}%
\begin{minipage}[t]{.5\textwidth}
  \centering
  \includegraphics[scale=0.5]{lpBirdTwo.png}
\end{minipage}
\end{figure}

The high pass version of the bird (with 128 added to each pixel value for a nicer visualization):
\begin{figure}[H]
\centering
\begin{minipage}[t]{.5\textwidth}
  \centering
  \includegraphics[scale=0.5]{hpPlaneTwo.png}
\end{minipage}%
\begin{minipage}[t]{.5\textwidth}
  \centering
  \includegraphics[scale=0.5]{hpPlaneTwoNice.png}
\end{minipage}
\end{figure}

\newpage

The following images show the intermediate steps in making the Einstein-Monroe hybrid shown before in Figure \ref{fig:downSampling}. \\

The low pass versions:
\begin{figure}[H]
\centering
\begin{minipage}[t]{.5\textwidth}
  \centering
  \includegraphics[scale=0.8]{lpEinstein.png}
\end{minipage}%
\begin{minipage}[t]{.5\textwidth}
  \centering
  \includegraphics[scale=0.8]{lpMonroe.png}
\end{minipage}
\end{figure}

The high pass version of Einstein (with 128 added to each pixel value for a nicer visualization):
\begin{figure}[H]
\centering
\begin{minipage}[t]{.5\textwidth}
  \centering
  \includegraphics[scale=0.8]{hpEinst.png}
\end{minipage}%
\begin{minipage}[t]{.5\textwidth}
  \centering
  \includegraphics[scale=0.8]{hpEinstNice.png}
\end{minipage}
\end{figure}

\end{document}